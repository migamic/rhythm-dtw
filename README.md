# Rhythm checker

![Example image](ExampleImage.png)

Project for the [AI&Music festival hackathon (EduHack)](https://hackaiandmusicfestival.github.io/#educational).

You can also check the [Devpost submission](https://devpost.com/software/rhythm-checker).

## Team Glitchers:
* [Anxo Lois Pereira Canovas](https://gitlab.com/ganxito)
* [Jaume Ros Alonso](https://gitlab.com/migamic)

## Description

This project aims to provide a useful tool for self-learning musicians by giving **feedback about the rhythm accuracy of a performance**. The student compares a recording of his/her performance with a reference (e.g. a teacher's recording, a video from the internet, etc.) and *Rhythm checker* highlights **which parts the rhythm were off and by how much**.

### How does it work

At the core of *Rhythm checker* is a **Dynamic Time Warping** algorithm that efficiently computes an alignment, which we then parse to obtain the displayed charts. The implementation of DTW is ours; prior to DTW we obtain the Mel Frequency Cepstral Coefficients with the [Librosa](https://librosa.org/) module; the final charts are displayed with [Altair](https://altair-viz.github.io/).

The program only deals with the recordings at a very low level, analyzing them in the waveform and frequency domains. It has no notion of music structure. It could also be used to compare speech signals and others, though we have not tested it.

### How to interpret the results

The resulting line chart displays the rhythm accuracy of the given performance compared to the reference audio. High values (in blue) mean that that particular section was too fast, while negative values (in orange) mean that it was slow. The ideal performance should keep the line as close to 0 as possible.

Note that the results are produced by an objective comparison with the reference; a different rhythm is not necessarily bad (e.g. it can be a conscious decision by the musician for artistic purposes). This tool should only be used as a guide for the student to help identify some mistakes that would have gone unnoticed otherwise.

The accuracy of the results is not perfect either. Some things to consider are:
* There can be spikes at the beginning or end of the recording. This is due to silence before and after the performance (both from the reference and student's versions).
* A constant line with a small value usually reliably indicates that the particular section of the performance had a uniform speed difference compared with the target. A quickly oscillating line of low amplitude between positive and negative values could be a sign of imperfections of the program.
* High spikes in the middle of the chart usually indicate that the program has been unable to align the reference and the student's versions, usually caused by some difference other than rhythm, such as a wrong note; values just before and after that spot might be unreliable. An overly long pause might also create such effect, in that case the program would be giving the correct results.



## Repository outline
* `notebooks` contains an IPython notebook with a usage example. We recommend starting from here.
* `src` contains the source code for the project.
* `data` contains sample data used in the notebook as example. The data was taken from YouTube.

## Usage

The code in the example notebook should be self-explanatory.

The easiest way to use the program is through the `Process` wrapper function from the `utils.py` file, that given two file paths computes everything and displays the final chart.

You can also call all the functions separately, as is also shown in the notebook.

### Usage tips
* Background noise, a difference in timbre, etc. should not be a problem in theory, but for the best results both reference and student's performances should be as clean and similar as possible.
* Increasing the `times` value for the `Vec2Bin` function might produce smoother results, but values too high will ignore subtler imperfections.
* A high `path_penaly` value for the `GetDTW` function might remove some unwanted low-amplitude oscillations, but it will also make the program overlook some subtle rhythm imperfections.
* The final line chart is not 100% accurate with the DTW output due to smoothing. The results should not be analyzed too closely, so it is not of much importance. In any case, the parameter `inter='steps'` to the `BarChart` function will remove the smoothing and give accurate data.
