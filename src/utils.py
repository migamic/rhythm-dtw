import librosa
from librosa.feature import mfcc
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import sys
import plots
import dtw


def ReadMusic(path):
    signal, rate = librosa.load(path)
    signal = np.interp(signal, (signal.min(), signal.max()), (-1, +1)) # Normalize to [-1,1]
    return rate, signal

def ExtractMfcc(signal, rate, ncoef = 20, n_fft=2048, hop_length=512):
    return mfcc(y=signal, sr=rate,n_mfcc=ncoef, n_fft=n_fft, hop_length=hop_length)

def PlotWave(signal, main="Signal Wave"):
    plt.figure(1)
    plt.title(main)
    plt.plot(signal)
    plt.show()

def Spectrograma(signal,rate,M=[],n_mels=128,fmax=8000,main='Mel-frequency spectrogram'):
    #fig1, (ax1, ax3) = plt.subplots(1,2,figsize=(50,20))
    #ax1.plot(signal)
    #ax1.set_title(main)

    D = librosa.amplitude_to_db(np.abs(librosa.stft(signal)), ref=np.max)

    #fig2, (ax2, ax3) = plt.subplots(1,2)
    #librosa.display.specshow(D, y_axis='linear', sr=rate, ax=ax2,fmax=fmax)
    #ax2.set_title('Linear-frequency power spectrogram')

    fig3, ax3 = plt.subplots(figsize=(50,20))
    S = librosa.feature.melspectrogram(y=signal, sr=rate,n_mels=n_mels,fmax=fmax)
    S_dB = librosa.power_to_db(S, ref=np.max)
    img = librosa.display.specshow(S_dB, x_axis='time',
                             y_axis='mel', sr=rate,
                             fmax=fmax, ax=ax3)
    ax3.set_title('Mel-scaled power spectrogram')
    
    if len(M) == 0 or True: return

    #fig4, ax4 = plt.subplots()
    img = librosa.display.specshow(M - M.min(), x_axis='time', sr=rate, ax=ax4)
    #fig4.colorbar(img, ax=ax4)
    ax4.set_title('Mfcc')

def Process(PathRef, PathComp):
    sr1, wav1 = ReadMusic(PathRef)
    sr2, wav2 = ReadMusic(PathComp)
    mf1 = ExtractMfcc(wav1, sr1)
    mf2 = ExtractMfcc(wav2, sr2)
    c, p = dtw.GetDTW(mf1, mf2)
    res = plots.Vec2Bin(p,sr2,times=1)
    return plots.BarChart(res)
