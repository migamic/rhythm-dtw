import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import altair as alt
import pandas as pd

alt.data_transformers.disable_max_rows()

def Vec2Bin(vec,rate,hop_length=512, times=0.025):

    nvec = np.zeros((len(vec),2))
    nvec[:,0] = vec 

    movs = 0
    for i in range(len(vec)):
        movs = movs +1 if vec[i] != 1. else movs
        nvec[i,1] = movs/rate*hop_length


    result = []
    i1 = 0
    t = 0
    while i1 != -1 :
        i2 = next((list(nvec[:,1]).index(x) for x in nvec[:,1] if x >= t+times), -1)
        r = [0,0]
        r[0] = np.sum(nvec[i1:min(len(nvec[:,0]),i2),0])
        i1 = i2
        r[1] = round(t,3)
        t = t + times
        result.append(r)
    
    df = pd.DataFrame(result,columns=['diff','seconds'])
    return df


def BarChart(r,inter='catmull-rom',by=True,bx=False,w=1360,h=500):
    Max = max(abs(max(r['diff'])), abs(min(r['diff'])))
    bar = alt.Chart(r).mark_area(
        interpolate=inter,
        #line=True
    ).transform_calculate(
        negative='datum.diff < 0',
        #time="toString(floor(datum.seconds/60)) + ':' + toString(datum.seconds%60)"
    ).encode(
        x="seconds:N",
        y=alt.Y("diff:Q",scale=alt.Scale(domain=[-Max, Max]), axis=alt.Axis(labels=False,title=" ")),
        color=alt.Color('negative:N', legend=None)
    ).interactive(bind_x=bx, bind_y=by)#.properties(
        #width=w,
        #height=h
    #)
    return bar

def SpectroDF(mfcc):
    c, r = mfcc.shape
    a = np.zeros((len(mfcc.flatten()),3))
    a[:,0] = (mfcc -mfcc.min()).flatten()
    a[:,1] = list(range(c)) * r
    b = []
    for i in range(r):
        b += [i] * c
    a[:,2] = b
    df = pd.DataFrame(a,columns=['coef','col','row'])
    return df

def MEL(signal,rate,n_mels=128,fmax=8000,hl=512):
    S = librosa.feature.melspectrogram(y=signal, sr=rate,n_mels=n_mels,fmax=fmax,hop_length=hl)
    S_dB = librosa.power_to_db(S, ref=np.max)
    return S_dB

def HM(coef):
    hm = alt.Chart(coef).mark_rect().encode(
        x='row:N',
        y='col:N',
        color=alt.Color('coef:Q', scale=alt.Scale(scheme="inferno")),
    )
    return hm

def Spectrograma(signal,rate,M=[],n_mels=128,fmax=8000,main='Mel-frequency spectrogram'):

    D = librosa.amplitude_to_db(np.abs(librosa.stft(signal)), ref=np.max)


    fig3, ax3 = plt.subplots(figsize=(50,20))
    S = librosa.feature.melspectrogram(y=signal, sr=rate,n_mels=n_mels,fmax=fmax)
    S_dB = librosa.power_to_db(S, ref=np.max)
    img = librosa.display.specshow(S_dB, x_axis='time',
                             y_axis='mel', sr=rate,
                             fmax=fmax, ax=ax3)
    ax3.set_title('Mel-scaled power spectrogram')
