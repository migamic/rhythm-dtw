'''
Functions for DTW computations

Using the pydtw package by shunsukeaihara:
 https://github.com/shunsukeaihara/pydtw
'''

import numpy as np
import scipy
from numba import jit


# Returns the path of the alignment. May not be unique
# -1 means comp is too slow
# 0 means comp is perfect
# 1 means comp is too fast
@jit(nopython=True)
def GetPath(D, path_penalty):
    i,j = D.shape
    assert(i >= 1 and j >= 1), 'Invalid DTW matrix dimensions'

    path=np.zeros(1)

    i, j = i-1, j-1
    while not (i <= 1 or j <= 1):

        diag = D[i-1,j-1]
        up = D[i-1,j]+path_penalty
        left = D[i,j-1]+path_penalty

        if diag <= up and diag <= left:
            i, j = i-1, j-1
            path = np.append(path, 0)
        elif up <= left:
            i -= 1
            path = np.append(path, 1)
        else:
            j -= 1
            path = np.append(path, -1)

    # Add residual increments
    path = np.append(path, np.ones(i-1))
    path = np.append(path, np.ones(j-1)*(-1))
    return np.flip(path)


# Returns the cost and path of DTW with the D matrix already initialized
@jit(nopython=True)
def FillDMatrix(x,y,D,path_penalty=0):
    r, c = x.shape[0], y.shape[0]

    for i in range(r):
        for j in range(c):
            min_prev = min(D[i, j], D[i+1, j]+path_penalty, D[i, j+1]+path_penalty)
            D[i+1, j+1] += min_prev



# Returns the DTW cost and path given two sequences
# X is the 'reference. Y is the 'student'
def GetDTW(x, y, dist='sqeuclidean', path_penalty=0):

    x = np.transpose(x)
    y = np.transpose(y)

    r, c = x.shape[0], y.shape[0]

    D = np.zeros((r + 1, c + 1))
    D[0, 1:] = np.inf
    D[1:, 0] = np.inf

    D[1:, 1:] = scipy.spatial.distance.cdist(x, y, dist)

    FillDMatrix(x,y,D,path_penalty)

    # Path will be a list of coordinates
    if len(x) == 1:
        path = [(0,i) for i in range(len(y))]
    elif len(y) == 1:
        path = [(i,0) for i in range(len(x))]
    else:
        path = GetPath(D, path_penalty)
    return D[-1, -1], path
